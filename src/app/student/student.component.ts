import { Component, OnInit } from '@angular/core';
import {ProduitService} from '../produit.service'
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MineurPipe} from '../mineur.pipe';

@Component({
  selector: 'student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.sass']
})
export class StudentComponent implements OnInit {
  error = '';
  nomCtrl: FormControl;
  fournisseurCtrl: FormControl;
  ageCtrl: FormControl;
  descriptionCtrl: FormControl;


  isHidden = true;
  produits = [];

  userForm: FormGroup;

  currentProduit ={};

  constructor(private produitService : ProduitService, fb: FormBuilder) {
    this.nomCtrl = fb.control('');
    this.fournisseurCtrl = fb.control('');
    this.ageCtrl = fb.control('');
    this.descriptionCtrl = fb.control('');

    this.userForm = fb.group({
      nom: this.nomCtrl,
      fournisseur: this.fournisseurCtrl,
      age: this.ageCtrl,
      description: this.descriptionCtrl,
    });
  } 

  suppr(produit){
    this.produitService.suppr(produit);
  }

  ngOnInit() {
    this.produits = this.produitService.list();
  }

  showFormulaire(){
    return this.isHidden;
  }

  //TODO - cacher le formulaire
  ajouterFormulaire(){
    this.isHidden = !this.isHidden;
  }

  register() {
    this.produitService.add(this.userForm.value)
  }
}
