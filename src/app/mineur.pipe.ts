import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mineur'
})
export class MineurPipe implements PipeTransform {
  mineur = true;

  transform(value: any, args?: any): any {
    if(value > 18){
      this.mineur = false;
    }
    return this.mineur;
  }

}
