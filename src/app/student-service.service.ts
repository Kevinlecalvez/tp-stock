import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StudentServiceService {

  students = [{
    nomDeClasse: "MBA1",
    nom: "Valdez",
    prenom: "Kevin",
    age: 24,
    filliere: "Developpeur",
    description: "description",
  },
  {
    nomDeClasse: "MBA1",
    nom: "Valdez",
    prenom: "Esteban",
    age: 21,
    filliere: "Developpeur",
    description: "Clasheur",
  },
  {
    nomDeClasse: "MBA1",
    nom: "Tofigh",
    prenom: "David",
    age: 24,
    filliere: "Developpeur",
    description: "Barman",
  },
  {
    nomDeClasse: "MBA1",
    nom: "Norris",
    prenom: "Chuck",
    age: 78,
    filliere: "Acteur",
    description: "Je mets les pieds où je veux et c'est souvent dans la gueule !",
  },
  {
    nomDeClasse: "MBA1",
    nom: "Walmart",
    prenom: "kid",
    age: 12,
    filliere: "Chanteur",
    description: "https://www.youtube.com/watch?v=fy2XDBbDrAs'",
  }];

  constructor() { }

  list(){
    return this.students;
  }

  add(studentAdd){
    this.students.push(studentAdd);
    console.log(this.students)
  }
}
