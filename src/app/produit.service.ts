import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

    produits = [{
        nom : "Pizza Reine",
        fournisseur : "pizza hut",
        emailFournisseur : "pizzahut@gmail.com",
        ingredients : "fromage , pate à pizza, jambon, sauce tomate",
        description : "Reine",
        age : 0,
        condition : "Chaud",
        prix : 10
    }, {
        nom : "Pizza 4 fromages",
        fournisseur : "pizza hut",
        emailFournisseur : "pizzahut@gmail.com",
        ingredients : "4 formage , pate à pizza, jambon, sauce tomate",
        description : "4 fromage",
        age : 0,
        condition : "Chaud",
        prix : 12
    },{
        nom : "Tarte à la fraise",
        fournisseur : "Carefour",
        emailFournisseur : "carefour@gmail.com",
        ingredients : "pâte a tart, fraise, crème",
        description : "Tarte aux fraises",
        age : 0,
        condition : "Froid",
        prix : 7
    },{
        nom : "Jambon",
        fournisseur : "carefour",
        emailFournisseur : "carefour@gmail.com",
        ingredients : "jambon de porc",
        description : "4 tranches de Jambon",
        age : 0,
        condition : "Frais",
        prix : 2
    },{
        nom : "Oeufs",
        fournisseur : "commercant",
        emailFournisseur : "commercant@gmail.com",
        ingredients : "oeuf",
        description : "10 oeufs frais",
        age : 0,
        condition : "Frais",
        prix : 1
    }, {
        nom : "Ferrari",
        fournisseur : "Spécialiste",
        emailFournisseur : "ferrari@gmail.com",
        ingredients : "Carosserie, moteur, roues",
        description : "LES PIONNIÈRES D’UN NOUVEAU CONCEPT DE VOITURES ICONA EN SÉRIE LIMITÉE", 
        age : 3,
        condition : "Tempérer",
        prix : 150000
    }];

  constructor() { }

  list(){
    return this.produits;
  }

  suppr(produitSuppr){
      console.log(produitSuppr)
    this.produits.forEach( (item, index) => {
        if(item === produitSuppr) this.produits.splice(index,1);
      });
      console.log(this.produits)
  }

  add(produitAdd){
    this.produits.push(produitAdd);
    console.log(this.produits)
  }
}
