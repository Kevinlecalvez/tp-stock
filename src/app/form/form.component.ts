import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {StudentServiceService} from '../student-service.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {
  error = '';
  nomDeClasseCtrl: FormControl;
  nomCtrl: FormControl;
  prenomCtrl: FormControl;
  ageCtrl: FormControl;
  filliereCtrl: FormControl;
  descriptionCtrl: FormControl;
  mailCtrl: FormControl;

  userForm: FormGroup;
  
  ngOnInit() {
  }

  constructor(private studentService : StudentServiceService, fb: FormBuilder) {
    this.nomDeClasseCtrl = fb.control('');
    this.nomCtrl = fb.control('', [Validators.required, Validators.maxLength(25)]);
    this.prenomCtrl = fb.control('', [Validators.required, Validators.maxLength(25)]);
    this.ageCtrl = fb.control('', [Validators.required, Validators.pattern('[1-9]*')]);
    this.filliereCtrl = fb.control('');
    this.descriptionCtrl = fb.control('');
    this.mailCtrl = fb.control('', Validators.email);

    this.userForm = fb.group({
      nomDeClasse: this.nomDeClasseCtrl,
      nom: this.nomCtrl,
      prenom: this.prenomCtrl,
      age: this.ageCtrl,
      filliere: this.filliereCtrl,
      description: this.descriptionCtrl,
      mail: this.mailCtrl,
    });
  }

  //methods
  reset() {
    this.nomDeClasseCtrl.setValue('');
    this.nomCtrl.setValue('');
    this.prenomCtrl.setValue('');
    this.ageCtrl.setValue('');
    this.filliereCtrl.setValue('');
    this.ageCtrl.setValue('');
    this.descriptionCtrl.setValue('');
    this.mailCtrl.setValue('');
  }

  register() {
    this.studentService.add(this.userForm.value)
  }
}
